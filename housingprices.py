# -*- coding: utf-8 -*-
"""
Created on Tue Oct 29 17:36:30 2019

@author: Tim
"""

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np


train = pd.read_csv('/Users/Tim/house-prices-advanced-regression-techniques/house-prices-using-advanced-regression-techniques/train.csv')
test = pd.read_csv('/Users/Tim/house-prices-advanced-regression-techniques/house-prices-using-advanced-regression-techniques/test.csv')

train.dtypes
train.head()

#get some basic info on sale price
train['SalePrice'].describe()

sns.distplot(train['SalePrice'], kde =False)
#positively skewed distribution

print("Skewness:", train['SalePrice'].skew())
print("Kurtosis: ",train['SalePrice'].kurt())


#train.plot.scatter(x='GrLivArea', y='SalePrice', ylim=(0,800000))


#This data has a huge amount of features. It is interesting therefore to try L1 regularisation in a linear model, because
#this will automatically set a lot of the weights to zero, and essentially shows which features are most important 
#(kind of a way of doing self selection of features to see where to start off)

#This is probably one of the few cases where doing some modelling before some data analysis makes sense


#making a data combined dataset with dummy variables for all categorical data
data_combined = pd.concat([train[train.columns[0:-1]], test])
data_combined = pd.get_dummies(data_combined)
data_combined = data_combined.fillna(data_combined.mean())


from sklearn.linear_model import Lasso
from sklearn.model_selection import cross_val_score, train_test_split, GridSearchCV


#Now need to make my train and test dataset from data combined
X_trainval = data_combined[:train.shape[0]]
X_test = data_combined[train.shape[0]:]
y_trainval = train['SalePrice']

X_train, X_val , y_train, y_val = train_test_split(X_trainval, y_trainval, train_size = 0.75)



param_grid = {'alpha' : [0.0001,0.001,0.01,0.1,1,10,100,1000,10000]}
grid_search = GridSearchCV(Lasso(), param_grid, cv=5)
grid_search.fit(X_train, y_train)

grid_search.best_params_
grid_search.score(X_val, y_val)

#Evaluating on the X_val dataset, which was not used to train the model, we got an R^2 value of 0.9, which is relatively good

#Now I have a number to compare to when making other models to see if they outperform Lasso

#Crucial that Lasso sets many of the coefficients to zero here, meaning it does some feature selection for us. 
#Interesting to take all of those features and use those for training other models, like ridge, randomforest etc.,
#see if potentially they give better results

#First going to submit current predictions and see how close 0.9 is to real value

#train on entire dataset before I do this
grid_search.fit(X_trainval, y_trainval)

y_test = grid_search.predict(X_test)

temp = pd.DataFrame(pd.DataFrame({'Id': test['Id'], 'SalePrice': y_test}))

temp.to_csv('basiclassosubmission.csv', index = False)


#Here I found that the scoring used by kaggle is different, they use RMSE instead of R^2. Going to change this one thing
#in my code and see if I will get a better score just by using the same performance metric as measured by kaggle

def rmsle(y_pred, y_test) : 
    assert len(y_test) == len(y_pred)
    return np.sqrt(np.mean((np.log(1+y_pred) - np.log(1+y_test))**2))

from sklearn.metrics import make_scorer

rmsle_score = make_scorer(rmsle, greater_is_better=False)
#Using rmsle as scoring this time
param_grid = {'alpha' : [0.0001,0.001,0.01,0.1,1,10,100,1000,10000]}
grid_search = GridSearchCV(Lasso(), param_grid, cv=5, scoring = rmsle_score, return_train_score=True)

#fitting model
grid_search.fit(X_train, y_train)
grid_search.score(X_val, y_val)

#train on whole dataset
grid_search.fit(X_trainval, y_trainval)

y_test = grid_search.predict(X_test)

temp = pd.DataFrame(pd.DataFrame({'Id': test['Id'], 'SalePrice': y_test}))

temp.to_csv('basiclassosubmission2.csv', index = False)


#Now going to look at which coefficients lasso decided were going to be set as zero, and use these to build other models
#and compare my results to this one

#The following gives the list of coefficients
grid_search.best_estimator_.coef_

#Need to plot this in a more convenient way to see which are non-zero
coefs = pd.DataFrame({'coefficients': grid_search.best_estimator_.coef_})
coefs['var'] = data_combined.columns

coefs = coefs.sort_values(by = 'coefficients')
coefs['coefficients'].value_counts().head()
#169 variables were set to 0, while the rest 120 were set to non-zero

#Going to make a list saving all of the most relevant parameters
relevant_params = pd.DataFrame(coefs['var'][coefs['coefficients']!=0])

#Now going to perform other Linear regression using these same variables, for example with ridge

from sklearn.linear_model import Ridge

ridge = Ridge()

basic_ridge_grid = GridSearchCV(ridge, cv = 5, scoring = rmsle_score, param_grid=param_grid)
basic_ridge_grid.fit(X_train,y_train)

basic_ridge_grid.score(X_val, y_val)

#fit on entire data
ridge_grid.fit(X_trainval,y_trainval)
y_test = ridge_grid.predict(X_test)

temp = pd.DataFrame(pd.DataFrame({'Id': test['Id'], 'SalePrice': y_test}))
temp.to_csv('ridgeregression)

